package com.example.SpringBootMicroService.service;

import com.example.SpringBootMicroService.model.InputRequest;
import com.example.SpringBootMicroService.model.OutResponse;

public interface PalindromicService {

   OutResponse savePalindromic(InputRequest inputRequest);
   String getPalindromicId(String userId);
}
