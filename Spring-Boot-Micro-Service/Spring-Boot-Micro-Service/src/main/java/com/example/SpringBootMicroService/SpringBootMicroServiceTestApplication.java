package com.example.SpringBootMicroService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMicroServiceTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMicroServiceTestApplication.class, args);
	}

}
