package com.example.SpringBootMicroService.controller;

import com.example.SpringBootMicroService.model.InputRequest;
import com.example.SpringBootMicroService.model.OutResponse;
import com.example.SpringBootMicroService.service.PalindromicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PalindromicController {

    @Autowired
    PalindromicService service;

    @PostMapping(value = "/save") //http://localhost:7071/api/save
    public OutResponse savePalindromic(@RequestBody InputRequest request) {
        OutResponse returnValue = service.savePalindromic(request);

        return returnValue;
    }

    @GetMapping(path = "/{id}") //http://localhost:7071/api/id
    public String getPalindromic(@PathVariable String id) {
        String returnValue = service.getPalindromicId(id);

        return returnValue;
    }
}
