package com.example.SpringBootMicroService.model;

public class OutResponse {

    private String str;
    private String userId;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
