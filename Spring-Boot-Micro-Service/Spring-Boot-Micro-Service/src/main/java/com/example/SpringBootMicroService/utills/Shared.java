package com.example.SpringBootMicroService.utills;

import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Random;

@Service
public class Shared {

    private final Random RANDOM = new SecureRandom();
    private final String ALPHABET = "02468acegikmoqsuwy13579BDFHJLNPRTVXZ";

    public String generateRandomString(int length) {
        StringBuilder returnValue = new StringBuilder();

        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return new String(returnValue);
    }
}
