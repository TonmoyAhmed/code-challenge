package com.example.SpringBootMicroService.service.impl;

import com.example.SpringBootMicroService.Entity.PalindromicEntity;
import com.example.SpringBootMicroService.model.InputRequest;
import com.example.SpringBootMicroService.model.OutResponse;
import com.example.SpringBootMicroService.repository.PalindromicRepository;
import com.example.SpringBootMicroService.service.PalindromicService;
import com.example.SpringBootMicroService.utills.LargestPalindromic;
import com.example.SpringBootMicroService.utills.Shared;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PalindromicServiceImpl implements PalindromicService {

    @Autowired
    LargestPalindromic largestPalindromic;

    @Autowired
    PalindromicRepository repository;

    @Autowired
    Shared shared;

    @Override
    public OutResponse savePalindromic(InputRequest inputRequest) {

        String result = largestPalindromic.longestPalindrome(inputRequest.getStr());

        PalindromicEntity entity = new PalindromicEntity();
        entity.setUserId(shared.generateRandomString(3));
        entity.setStr(result);

        PalindromicEntity palindromicEntity = repository.save(entity);
        OutResponse responce = new OutResponse();
        BeanUtils.copyProperties(palindromicEntity, responce);

        return responce;
    }

    @Override
    public String getPalindromicId(String userId) {

        PalindromicEntity entity = repository.findByUserId(userId);

        return entity.getStr();
    }


}
