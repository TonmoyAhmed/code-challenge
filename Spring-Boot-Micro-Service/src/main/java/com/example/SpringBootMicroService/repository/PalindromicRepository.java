package com.example.SpringBootMicroService.repository;

import com.example.SpringBootMicroService.Entity.PalindromicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PalindromicRepository extends PagingAndSortingRepository<PalindromicEntity, Long> {

    PalindromicEntity findByUserId(String id);

}
