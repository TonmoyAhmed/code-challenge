package com.example.SpringBootMicroService.model;

public class InputRequest {

    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
